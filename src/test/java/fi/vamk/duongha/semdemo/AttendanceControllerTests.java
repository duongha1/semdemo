package fi.vamk.duongha.semdemo;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.format.DateTimeFormatters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = { "fi.vamk.e1800917.SEMdemo" })
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)

public class AttendanceControllerTests {
  @Autowired
  private AttendanceRepository repository;

  @Test
  public void postGetDeleteAttendance() {
    Iterable<Attendance> begin = repository.findAll();
    System.out.println(IterableUtils.size(begin));
    // given
    Attendance attendance = new Attendance("ahihi");
    System.out.println(attendance.toString());
    // test save
    Attendance savedOne = repository.save(attendance);
    System.out.println(savedOne.toString());
    // when
    Attendance foundOne = repository.findByKey(attendance.getKey());
    System.out.println(foundOne.toString());
    // then
    assertThat(foundOne.getKey()).isEqualTo(attendance.getKey());
    repository.delete(foundOne);
    Iterable<Attendance> end = repository.findAll();
    System.out.println(IterableUtils.size(end));
    assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
  }

  @Test
  public void addFetchByDate() {
    Attendance a1 = new Attendance("uhuhu");
    a1.setDate(Instant.parse("2010-10-02T12:23:23Z"));
    System.out.println("a1 " + a1.toString());
    Attendance savedOne = repository.save(a1);
    System.out.println("save " + savedOne.toString());
    Attendance foundOne = repository.findByDate(Instant.parse("2010-10-02T12:23:23Z"));
    System.out.println("found " + foundOne.toString());
    assertEquals(a1.toString(), foundOne.toString());
  }
}

