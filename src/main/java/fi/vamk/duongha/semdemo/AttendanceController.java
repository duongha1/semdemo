package fi.vamk.duongha.semdemo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AttendanceController {
  @Autowired
  private AttendanceRepository repository;

  @CrossOrigin
  @GetMapping("/attendances")
  public Iterable<Attendance> list() {
    return repository.findAll();
  }

  @GetMapping("/attendace/{id}")
  public Optional<Attendance> get(@PathVariable("id") int id) {
    return repository.findById(id);
  }

  @PostMapping("/attendance")
  public @ResponseBody Attendance create(@RequestBody Attendance item) {
    if (item.getDate() == null) {
      System.out.println("Cant save due to the lack of date");
      return repository.save(null);
    } else {

      return repository.save(item);
    }
  }

  @PutMapping("/attendance")
  public @ResponseBody Attendance update(@RequestBody Attendance item) {
    if (item.getDate() == null) {
      System.out.println("Cant save due to the lack of date");
      return repository.save(null);
    } else {

      return repository.save(item);
    }
  }

  @DeleteMapping("/attendance")
  public void delete(@RequestBody Attendance item) {
    repository.delete(item);
  }
}
