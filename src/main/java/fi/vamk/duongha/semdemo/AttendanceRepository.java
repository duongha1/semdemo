package fi.vamk.duongha.semdemo;

import java.time.Instant;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
  public Attendance findByKey(String key);

  public Attendance findByDate(Instant date);
}
